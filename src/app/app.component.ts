import { Component } from '@angular/core';
import { RegUser } from './shared/reg-user'
import { UserRegistrationService } from './services/reg-service.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  user= null;
  gender: string [] = [ "M", "F"];
  age: number;
  


  nationalities = [
    {desc:"", value:""},
    {desc:'Singaporean', value:'SG'}, 
    {desc: 'Malaysian', value:'MY'}, 
    {desc:'Thai', value:'TH'},  
    {desc:'Indonesian', value: 'IN'}
  ];

  isSubmitted: boolean = false;
  users: any;

  constructor (private userService: UserRegistrationService){ }

  ngOnInit () {
    this.user= new RegUser ('','','','',null,"", "", "");
  }

onSubmit(){
  console.log (this.user.name);
  console.log (this.user.password);
  console.log (this.user.nationalities);
  console.log (this.user.gender);
  console.log (this.user.dateOfBirth);
  console.log (this.user.address);
  console.log (this.user.mobile);
  console.log (this.user.email);
  this.isSubmitted = true;

  this.userService.saveUserRegistration(this.user)
  .subscribe(users => {
    console.log('send to backend !');
    console.log(users);
    this.users = users;
  })

  function onChange (event)
  {
    console.log (event)
      if(this.user.dateOfBirth){
      var timeDiff = Math.abs(Date.now() - this.user.dateOfBirth);
      this.age = Math.floor((timeDiff / (1000 * 3600 * 24))/365);
         
     }
 }

  }

}
}
