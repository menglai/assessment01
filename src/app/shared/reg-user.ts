export class RegUser {

    constructor (
        public name: string,
        public password: string,
        public nationality: string,
        public gender: string,
        public dateOfBirth: Date,
        public address: string,
        public mobile: string,
        public email: string,
    ){

    }
}
