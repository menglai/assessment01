import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

import { RegUser } from '../shared/reg-user';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable()
export class UserRegistrationService {
    private userAPIURL = `${environment.backendApiURL}/api/user/`
  
  constructor(private httpClient: HttpClient) { }

  public saveUserRegistration(user: RegUser) : Observable<RegUser>{
    return this.httpClient.post< RegUser >(this.userAPIURL + 'register', user, httpOptions);
  }

  }